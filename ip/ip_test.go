package ip

import (
	"log"
	"testing"
)

func TestGetZip(t *testing.T) {
	zip, err := GetZipAndCountry()
	if err != nil {
		log.Println(err.Error())
		t.Fail()
	}
	log.Println(zip)
}
