package ip

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type InfoOut struct {
	Postal  string
	City    string
	Region  string
	Country string
}

func GetZipAndCountry(debug bool) (zip, country string, err error) {
	// Get current IP
	resp, err := http.Get("https://ipinfo.io/ip")
	if err != nil {
		return
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}
	// log.Println("BODY!")
	// log.Println(string(body))
	ip := string(body)
	if debug {
		fmt.Println("ip: ", ip)
	}

	resp, err = http.Get(fmt.Sprintf("https://ipinfo.io/%s", ip))
	if err != nil {
		return
	}
	i := InfoOut{}
	err = json.NewDecoder(resp.Body).Decode(&i)
	if err != nil {
		return
	}
	if debug {
		fmt.Println("ipinfo.io", i)
	}
	return i.Postal, i.Country, nil
}
