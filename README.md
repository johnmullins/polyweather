# polyweather
## Why?
I made this app because I could not find a polybar weather module that I liked.
## How?
This app uses the [OpenWeatherMap API](https://openweathermap.org/api) to get the weather data.

It first gets your ip address. Then geoip is used to get your location. The location is then used to get the weather data from the OpenWeatherMap API.

The weather data is then parsed and formatted to be displayed in polybar.

## Building
### Dependencies
- Golang 1.19 or higher
- [WeatherIcons](https://erikflowers.github.io/weather-icons/)
- A file in your home directory called `.openweather` which contains your OpenWeatherMap API key

### Building
```bash
go build -o polyweather
chmod +x polyweather
```

### Polybar configuration
Inside of you ~/config/polybar/config.ini file add the following module:
```ini
[module/weather]
    type = custom/script
    exec = "/path/to/polyweather"
    tail = false
    interval = 60
    label-font=2
```

Ensure WeatherIcons is in your font list
```ini
font-1 = WeatherIcons:size=12;1
```

Add the module to your polybar
```ini
modules-center = weather
```

## Options
Passing `--short` to the executable will give a short version which includes only the icon and degrees