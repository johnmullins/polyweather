package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/johnmullins/polyweather/icons"
	"gitlab.com/johnmullins/polyweather/ip"
	"net/http"
	"os"
	"path"
	"strings"
)

type OpenWeatherOutput struct {
	Coord      Coord     `json:"coord"`
	Weather    []Weather `json:"weather"`
	Base       string    `json:"base"`
	Main       Main      `json:"main"`
	Visibility int       `json:"visibility"`
	Wind       Wind      `json:"wind"`
	Clouds     Clouds    `json:"clouds"`
	Dt         int       `json:"dt"`
	Sys        Sys       `json:"sys"`
	Timezone   int       `json:"timezone"`
	ID         int       `json:"id"`
	Name       string    `json:"name"`
	Cod        int       `json:"cod"`
}
type Coord struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}
type Weather struct {
	ID          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}
type Main struct {
	Temp      float64 `json:"temp"`
	FeelsLike float64 `json:"feels_like"`
	TempMin   float64 `json:"temp_min"`
	TempMax   float64 `json:"temp_max"`
	Pressure  int     `json:"pressure"`
	Humidity  int     `json:"humidity"`
}
type Wind struct {
	Speed float64 `json:"speed"`
	Deg   int     `json:"deg"`
}
type Clouds struct {
	All int `json:"all"`
}
type Sys struct {
	Type    int    `json:"type"`
	ID      int    `json:"id"`
	Country string `json:"country"`
	Sunrise int    `json:"sunrise"`
	Sunset  int    `json:"sunset"`
}

func main() {
	args := os.Args[1:]
	short := false
	debug := false
	if len(args) > 0 {
		for _, a := range args {
			a = strings.Replace(a, "-", "", -1)
			switch a {
			case "short":
				short = true
			case "debug":
				debug = true
			}
		}
	}

	home, err := os.UserHomeDir()
	if err != nil {
		fmt.Println("could not get home dir")
		return
	}
	apiKeyPath := path.Join(home, ".openweather")
	apiFile, err := os.ReadFile(apiKeyPath)
	if err != nil {
		fmt.Println("could not read api key in" + apiKeyPath)
		return
	}
	apiKey := strings.TrimSpace(string(apiFile))
	if debug {
		fmt.Println("api key: ", apiKey)
	}

	zip, country, err := ip.GetZipAndCountry(debug)
	if err != nil {
		fmt.Println("where are you?")
		return
	}

	if zip == "" || country == "" {
		fmt.Println("Could not geolocate")
		return
	}

	switch zip {
	case "33910":
		zip = "34114"
	case "33598":
		zip = "33578"
	}

	url := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?appid=%s&units=imperial&lang=en&q=%s,%s", apiKey, zip, country)

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("could not get weather")
		return
	}

	if debug {
		fmt.Println("response status code: ", response.StatusCode)
	}

	if response.StatusCode != http.StatusOK {
		//fmt.Println("openweather up?")
		fmt.Println("Not ok: ", response.StatusCode, " ", zip, " ", country)
		return
	}
	a := OpenWeatherOutput{}
	err = json.NewDecoder(response.Body).Decode(&a)
	if err != nil {
		fmt.Println("could not decode json")
		return
	}
	if debug {
		fmt.Println(a)
	}
	var temp = int(a.Main.Temp)
	name := a.Name
	if name == "Brandon" {
		name = "Riverview"
		if debug {
			fmt.Println("changing name to Riverview")
		}
	}
	icon := icons.IconMap[a.Weather[0].Icon]
	outString := fmt.Sprintf("%s %s %s %d° %s", icon, name, country, temp, a.Weather[0].Description) // icon in front
	if country == "US" {
		outString = fmt.Sprintf("%s %s %d° %s", icon, name, temp, a.Weather[0].Description) // icon in front
	}

	// outString := fmt.Sprintf("%s %d° %s %s", a.Name, temp, icon, a.Weather[0].Description) // icon in middle
	// outString := fmt.Sprintf("%s %d° %s %s", a.Name, temp, a.Weather[0].Description, icon) //icon at end

	if short {
		outString = fmt.Sprintf("%s %d°", icon, temp)
	}
	fmt.Println(outString)
}
